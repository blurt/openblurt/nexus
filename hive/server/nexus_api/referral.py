"""Nexus API: Notifications"""
import logging

from hive.server.common.helpers import return_error_info, json_date
from hive.indexer.notify import NotifyType
from hive.server.hive_api.common import get_account_id, valid_limit, get_post_id

@return_error_info
async def referral_accounts(context, referrer=None, campaign_id=None, last_created_at=None, limit=100):
    """Load affiliate accounts with keyset pagination."""
    limit = valid_limit(limit, 1000)
    db = context['db']

    # If referrer is provided, get the referrer_id, else set referrer_id to None
    referrer_id = await get_account_id(db, referrer) if referrer else None

    # Adjust the SQL query to use keyset pagination
    sql = """SELECT acc.name account, ref.name referrer , rf.campaign_id, rf.created_at FROM nexus_referral rf
          JOIN hive_accounts acc ON acc.id = rf.account_id
          JOIN hive_accounts ref ON ref.id = rf.referrer_id
          WHERE (:referrer_id IS NULL or rf.referrer_id = :referrer_id)
          AND (:campaign_id IS NULL or rf.campaign_id = :campaign_id)
          AND (:last_created_at IS NULL OR rf.created_at < :last_created_at)
          ORDER BY rf.created_at DESC
          LIMIT :limit"""

    rows = await context['db'].query_all(
        sql,
        referrer_id=referrer_id,
        campaign_id=campaign_id,
        last_created_at=last_created_at,
        limit=limit
    )

    return [_render(row) for row in rows]

@return_error_info
async def referral_accounts_count(context, referrer=None, campaign_id=None, start_date=None, end_date=None):
    """Get the count of affiliate accounts based on optional parameters."""

    db = context['db']

    # If referrer is provided, get the referrer_id, else set referrer_id to None
    referrer_id = await get_account_id(db, referrer) if referrer else None

    # Build the SQL query based on provided parameters
    sql = """SELECT COUNT(*) FROM nexus_referral rf
          WHERE (:referrer_id IS NULL OR rf.referrer_id = :referrer_id)
          AND (:campaign_id IS NULL or rf.campaign_id = :campaign_id)
          AND (:start_date IS NULL OR rf.created_at >= :start_date)
          AND (:end_date IS NULL OR rf.created_at <= :end_date)"""

    # Execute the query and fetch the count
    count = await context['db'].query_one(
        sql,
        referrer_id=referrer_id,
        campaign_id=campaign_id,
        start_date=start_date,
        end_date=end_date
    )

    return {'referrer': referrer, 'campaign_id': campaign_id, 'count': count}

def _render(row):
    """Convert object to string rep."""
    # src dst payload community post
    out = { 'account': row['account'], 'referrer': row['referrer'], 'campaign_id': row['campaign_id'], 'created': str(row['created_at']) }
    return out