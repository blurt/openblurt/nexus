"""Genesis snapshot importing"""

import json
import logging

log = logging.getLogger(__name__)

def get_snapshot_accounts_query(snapshot_path):
    result = []
    try:
        f = open(snapshot_path,'r')
        snapshot_accounts = f.readlines()
        for account in snapshot_accounts:
            account_obj = json.loads(account.strip())
            result.append("INSERT INTO hive_accounts (name, created_at) VALUES ('{}', '2020-07-04 01:50:12')".format(account_obj['name']))
        f.close()
        log.info('Parsed {} genesis accounts'.format(len(result)))
    except FileNotFoundError:
        log.warn('Snapshot file does not exist, skipping genesis accounts import...')
    except Exception:
        log.warn('Invalid snapshot file, not importing any genesis accounts')
        result = []
    return result