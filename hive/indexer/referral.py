"""[WIP] Process community ops."""

#pylint: disable=too-many-lines

import logging
from datetime import datetime, timedelta

from hive.db.adapter import Db
from hive.indexer.accounts import Accounts
from hive.indexer.notify import Notify
from hive.db.db_state import DbState

log = logging.getLogger(__name__)

DB = Db.instance()

class Referral:
    """Handles processing of incoming referral to db."""

    @classmethod
    def referral_op(cls, account, op_json, date):
        """Process an incoming referral op."""
        op = cls._validated_op(account, op_json, date)
        if not op:
            return  

        log.info("referral %s for account %s",
                    op_json['referrer'], account)         
        
        # insert
        sql = """INSERT INTO nexus_referral (account_id, referrer_id,
                 campaign_id, created_at) VALUES (:account, :referrer, :campaign, :at)"""
        DB.query(sql, **op)

        # notify
        if not DbState.is_initial_sync():
            
            # score = Accounts.default_score(account)
            score = 50
            Notify('referral', src_id=op['account'], dst_id=op['referrer'],
                    when=op['at'], score=score).write()

    @classmethod
    def _validated_op(cls, account, op, date):
        """Validate and normalize the operation."""
        if(not 'referrer' in op or not 'campaign' in op):
            return None
        
        account_id = Accounts.get_id(account)

        if(not Accounts.exists(op['referrer'])
           or cls._get_referral(account_id) # invalid account or existing referrer
           or cls._old_account(account_id, date)): # invalid account too old
            return None
        
        return dict(account=account_id,
                    referrer=Accounts.get_id(op['referrer']),
                    campaign=op['campaign'],
                    at=date)

    @classmethod
    def _get_referral(cls, account):
        sql      = "SELECT referrer_id FROM nexus_referral WHERE account_id = :id"
        referrer = DB.query_one(sql, id=account)
        return referrer
    
    @classmethod
    def _old_account(cls, account, date):
        sql = "SELECT created_at FROM hive_accounts WHERE id = :id"
        created_at = DB.query_one(sql, id=account)

        # Convert database dates to datetime format
        created_at_datetime = datetime.strptime(str(created_at), "%Y-%m-%d %H:%M:%S")
        date_datetime = datetime.strptime(date, "%Y-%m-%dT%H:%M:%S")

        # Calculate the difference between the two dates
        time_difference = date_datetime - created_at_datetime

        # Check if the difference is greater than 1 hour
        if time_difference > timedelta(hours=1):
            # The difference is more than 1 hour
            return True
        else:
            # The difference is less than or equal to 1 hour
            return False