#!/bin/bash

# NOTE: this script will be executed again if Nexus crashes or aborts. This
# could happen in the case of an unexpected upstream/API response, or when
# a non-micro-fork was encountered. Nexus has a startup routine which attempts
# to recover automatically, so database should be kept intact between restarts.

# hive expects: DATABASE_URL, LOG_LEVEL, STEEMD_URL
# default DATABASE_URL should be postgresql://postgres:postgres@localhost:5432/postgres

POPULATE_CMD="$(which hive)"


# start Nexus with an external postgres
cd $APP_ROOT

echo Nexus: starting sync.
exec "${POPULATE_CMD}" sync 2>&1&
echo Nexus: starting server.
exec "${POPULATE_CMD}" server 2>&1&

while :
do
  HIVESYNC_PID=`pgrep -f 'hive sync'`

  if [[ ! $? -eq 0 ]]; then
      echo "NOTIFYALERT! Nexus sync quit unexpectedly; restarting Nexus sync..."
      cd $APP_ROOT
      exec "${POPULATE_CMD}" sync 2>&1&
  fi

  sleep 30

  HIVESERVER_PID=`pgrep -f 'hive server'`

  if [[ ! $? -eq 0 ]]; then
      echo "NOTIFYALERT! Nexus server quit unexpectedly; restarting Nexus server..."
      cd $APP_ROOT
      exec "${POPULATE_CMD}" server 2>&1&
  fi

  # prevent flapping
  sleep 110
done

echo Nexus: application has stopped. See log for errors.
