FROM phusion/baseimage:focal-1.2.0


ENV LOG_LEVEL INFO
ENV LANG en_US.UTF-8
ENV LC_ALL en_US.UTF-8
ENV PIPENV_VENV_IN_PROJECT 1
ENV SNAPSHOT_PATH /app/genesis_snapshot/snapshot.json
ENV APP_ROOT /app
ENV HTTP_SERVER_PORT 8092

RUN \
    apt-get update && \
    apt-get upgrade -y -o Dpkg::Options::="--force-confold" && \
    apt-get install -y wget curl software-properties-common && \
    add-apt-repository -y ppa:deadsnakes/ppa && \
    apt-get update && \
    apt-get install -y \
    build-essential \
    libmysqlclient-dev \
    libffi-dev \
    libssl-dev \
    libxml2-dev \
    libxslt-dev \
    libpcre3 \
    libpcre3-dev \
    python3.8 \
    python3.8-dev \
    python3.8-venv \
    python3.8-distutils \
    python3.8-lib2to3 \
    python3.8-gdbm && \
    wget https://bootstrap.pypa.io/get-pip.py && \
    python3.8 get-pip.py && \
    python3.8 -m pip install --upgrade pip setuptools

ADD . /app

WORKDIR /app

# ADD scripts/syncstart.sh /etc/service/syncstart/run
# ADD scripts/serverstart.sh /etc/service/serverstart/run
# RUN chmod +x /etc/service/syncstart/run
# RUN chmod +x /etc/service/serverstart/run

# Download genesis snapshot
RUN \
    mkdir genesis_snapshot && \
    wget -O ./genesis_snapshot/snapshot.json https://blurt-chain.s3.nl-ams.scw.cloud/snapshot.json

ADD scripts/entrypoint.sh /usr/local/bin/entrypoint.sh
RUN chmod +x /usr/local/bin/entrypoint.sh

RUN \
    python3.8 -m pip install . && \
    apt-get remove -y \
    build-essential \
    libffi-dev \
    libssl-dev && \
    apt-get autoremove -y && \
    apt-get clean && \
    rm -rf \
    /root/.cache \
    /var/lib/apt/lists/* \
    /tmp/* \
    /var/tmp/* \
    /var/cache/* \
    /usr/include \
    /usr/local/include

EXPOSE ${HTTP_SERVER_PORT}

CMD ["/sbin/my_init", "--", "/usr/local/bin/entrypoint.sh"]